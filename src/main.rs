// Copyright (C) 2019 Sebastian Schüpbach
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

//! Command line tool for analyzing MARC-XML records
//!
//! ## Usage
//!
//! ```sh
//! marcgrep [FLAGS] <PATTERNS>
//! ```
//!
//! `marcgrep` reads MARC-XML records from `STDIN` expecting one record per line. It scans the record for one or more patterns and prints the contents of the matching (sub)fields in one of the following forms:
//!
//! - As values (flag `-v`, `--value`)
//! - As counts, one pattern per line (flag `-c`, `--count`)
//! - As counts, one record per line (flag `-t`, `--tabular`)
//!
//! The indvidual elements on one line are tab-separated.
//!
//! A pattern can either be a full path to a (sub)field like `LDR`, `003`, `1001 a` (notice the missing `$` sign and the whitespace) or a part of a path. You can use question marks (`?`) to add a wildcard for a single position or simply leave out trailing characters to encompass a larger range of (sub)fields. For instance:
//!
//! - `100`: Matches all subfields and indicator combinations of field `100`
//! - `00`: Matches all controlfields (i.e. fields beginning with `00`)
//! - `100? `: Matches all subfields in field `100` with an empty second indicator and arbitrary subfield code
//! - `1000?a`: Matches all subfields in field `100` with first indicator equals `0` and subfield code `a`
//!
//! _Caution_: Wildcards and partial parts don't work with the tabular (flag `-t`) representation of data. In this case a pattern is taken as-is, i.e. a pattern like `100` won't match anything! The reason for this behaviour is the stream-based approach of reading the input data: The application can't foresee every existing instance of a partial / wildcard pattern in only a single pass, so a tabular representation is not possible.
//!
//! Typically, you will print a MARC-XML collection with either `(z)cat` or `curl` and pipe `STDOUT` to `marcgrep`. Each representation of matching (sub)fields in marcgrep has its one purposes. A few hints:
//!
//! - value representation (`-v`): Check the correctness of values of certain (sub)fields
//! - count representation (`-c`): Count the total of records comprising a certain amount of a (sub)field
//! - tabular representation (`-t`): Check if certain combinations of (sub)fields exist in records; show records which don't contain a certain field as well
//!
//! Useful tools to postprocess the data include `awk`, `sed`, `cut`, `grep`, `wc`, `sort` and `uniq`.
//!
//! ## Examples
//!
//! ### Filter combinations with awk
//!
//! ```sh
//! zcat an_example_file.xml.gz | marcgrep -t '1001 a' '7001 a' | awk '$2>0 && $3<1 {print $0}'
//! ```
//!
//! In this example, awk prints only lines in which the element in the second (tab-separated) column (= value of subfield `1001 a`) is greater than 0 and the element in the third column (= value of subfield `7001 a`) is lesser than 1.

#[macro_use]
extern crate lazy_static;

use clap::{App, Arg, ArgGroup};
use quick_xml::events::attributes::Attributes;
use quick_xml::events::Event;
use quick_xml::Reader;
use std::collections::HashMap;
use std::fmt;
use std::io;
use std::io::{BufRead, Write};
use std::process;
use std::str;

const CODE: &[u8] = &[99, 111, 100, 101];
const IND1: &[u8] = &[105, 110, 100, 49];
const IND2: &[u8] = &[105, 110, 100, 50];
const TAG: &[u8] = &[116, 97, 103];

lazy_static! {
    static ref FIELD001: FieldPath = FieldPath::from_bytes(&[48, 48, 49]);
}

fn main() {
    let matches = App::new("marcgrep")
        .version("0.3")
        .author("Sebastian Schüpbach <post@annotat.net>")
        .about("greps MARC-XML records")
        .arg(
            Arg::with_name("count")
                .required(false)
                .short("c")
                .long("count")
                .help("counts categories of fields / subfields (one line per category)")
                .takes_value(false),
        )
        .arg(
            Arg::with_name("tabular")
                .required(false)
                .short("t")
                .long("tabular")
                .help("counts categories of fields / subfields (one line per record)")
                .takes_value(false),
        )
        .arg(
            Arg::with_name("value")
                .required(false)
                .short("v")
                .long("value")
                .help("prints values of fields / subfields")
                .takes_value(false),
        )
        .arg(
            Arg::with_name("record")
                .required(false)
                .short("r")
                .long("record")
                .help("prints whole record")
                .takes_value(false),
        )
        .arg(
            Arg::with_name("pattern")
                .required(true)
                .index(1)
                .multiple(true)
                .help("MARC-XML patterns"),
        )
        .group(
            ArgGroup::with_name("display")
                .args(&["count", "record", "tabular", "value"])
                .required(true),
        )
        .get_matches();

    let patterns = matches
        .values_of("pattern")
        .unwrap()
        .map(|p| p.as_bytes())
        .map(FieldPath::from_bytes)
        .collect::<Vec<FieldPath>>();

    let mut cacher: Box<dyn ValueCache> = if matches.is_present("count") {
        Box::new(PatternValues::new(true))
    } else if matches.is_present("tabular") {
        let paths = patterns
            .iter()
            .map(|f| format!("{}", f))
            .collect::<Vec<String>>();
        print!("Record ID");
        paths.iter().for_each(|p| print!("\t{}", p));
        println!();
        Box::new(RecordValues::new(paths))
    } else {
        Box::new(PatternValues::new(false))
    };

    let stdin = io::stdin();
    let stdin = stdin.lock();
    let stdout = io::stdout();
    let mut buf = Vec::with_capacity(16484);
    let mut current_field = FieldPath::new();
    let mut stdout_writer = io::BufWriter::new(stdout);
    stdin.lines().for_each(|line| {
        if let Ok(l) = line {
            if is_record(&l) {
                let extracted_values = parse_line(
                    &l,
                    &patterns,
                    &mut *cacher,
                    &mut current_field,
                    &mut buf,
                    matches.is_present("record"),
                );
                write!(stdout_writer, "{}", extracted_values).unwrap_or_else(|_| process::exit(1));
                buf.clear();
            }
        } else {
            eprintln!("Error reading line!");
            stdout_writer.flush().unwrap();
            process::exit(1);
        }
    });
    stdout_writer.flush().unwrap();
}

fn is_record(line: &str) -> bool {
    line.starts_with("<record")
}

fn parse_line(
    line: &str,
    patterns: &[FieldPath],
    cacher: &mut dyn ValueCache,
    field: &mut FieldPath,
    buf: &mut Vec<u8>,
    print_record: bool,
) -> String {
    let mut reader = Reader::from_str(line);
    reader.trim_text(true);

    loop {
        match reader.read_event(buf) {
            Ok(Event::Start(ref e)) => {
                match get_type_of_field(e.name()) {
                    FieldType::LEADER => {
                        field.set_leader();
                    }
                    FieldType::CONTROLFIELD => {
                        field.set_controlfield(e.attributes());
                    }
                    FieldType::DATAFIELD => {
                        field.set_datafield(e.attributes());
                    }
                    FieldType::SUBFIELD => field.set_subfield(e.attributes()),
                    FieldType::OTHER => (), // Ignore other tags
                }
            }
            Ok(Event::Text(ref text)) => {
                if is_match(&field, patterns) {
                    (*cacher).add(
                        format!("{}", &field),
                        text.unescape_and_decode(&reader).unwrap(),
                    );
                    if print_record {
                        return line.to_owned();
                    }
                }
                if *field == *FIELD001 && !print_record {
                    (*cacher).add_rec_id(text.unescape_and_decode(&reader).unwrap());
                }
            }
            Ok(Event::Eof) => break,
            Err(e) => {
                eprintln!("WARN: {}", e);
            }
            Ok(_) => (), // Ignore other events
        }
    }
    if cacher.has_content() {
        cacher.flush()
    } else {
        String::new()
    }
}

fn get_type_of_field(name: &[u8]) -> FieldType {
    match name {
        b"leader" => FieldType::LEADER,
        b"controlfield" => FieldType::CONTROLFIELD,
        b"datafield" => FieldType::DATAFIELD,
        b"subfield" => FieldType::SUBFIELD,
        _ => FieldType::OTHER,
    }
}

fn is_match(field: &FieldPath, patterns: &[FieldPath]) -> bool {
    patterns.iter().any(|p| p == field)
}

fn get_tag<'a>(attrs: &'a mut Attributes) -> Result<[u8; 3], &'static str> {
    for attr in attrs {
        if let Ok(a) = attr {
            if a.key == TAG {
                let mut tag: [u8; 3] = [0; 3];
                let value = a.value.as_ref();
                for i in 0..3 {
                    tag[i] = value[i];
                }
                return Ok(tag);
            }
        }
    }
    Err("attribute not found")
}

fn get_tag_with_inds<'a>(attrs: &'a mut Attributes) -> Result<[u8; 5], &'static str> {
    let path = attrs.fold([0; 5], |acc, attr| {
        if let Ok(a) = attr {
            match a.key {
                TAG => {
                    let value = a.value.as_ref();
                    [value[0], value[1], value[2], acc[3], acc[4]]
                }
                IND1 => [acc[0], acc[1], acc[2], a.value.as_ref()[0], acc[4]],
                IND2 => [acc[0], acc[1], acc[2], acc[3], a.value.as_ref()[0]],
                _ => acc,
            }
        } else {
            acc
        }
    });
    Ok(path)
}

fn get_code<'a>(attrs: &'a mut Attributes) -> Result<u8, &'static str> {
    for attr in attrs {
        if let Ok(a) = attr {
            if a.key == CODE {
                return Ok(a.value.as_ref()[0]);
            }
        }
    }
    Err("attribute not found")
}

#[derive(Debug)]
struct FieldPath {
    path: [u8; 6],
}

impl FieldPath {
    fn new() -> Self {
        FieldPath { path: [0; 6] }
    }

    fn from_bytes(bytes: &[u8]) -> Self {
        if bytes.len() > 6 || bytes.len() < 3 {
            eprintln!("ERROR: Path to MARC field is not valid!");
            process::exit(1);
        }
        let mut path: [u8; 6] = [0; 6];
        for i in 0..bytes.len() {
            path[i] = bytes[i];
        }
        FieldPath { path }
    }

    fn set_leader(&mut self) {
        self.path = [114, 104, 122, 0, 0, 0];
    }

    fn set_controlfield(&mut self, mut attrs: Attributes) {
        if let Ok(tag) = get_tag(&mut attrs) {
            self.path[0] = tag[0];
            self.path[1] = tag[1];
            self.path[2] = tag[2];
            self.path[3] = 0;
            self.path[4] = 0;
            self.path[5] = 0;
        }
    }

    fn set_datafield(&mut self, mut attrs: Attributes) {
        if let Ok(values) = get_tag_with_inds(&mut attrs) {
            for i in 0..5 {
                self.path[i] = values[i];
            }
        }
    }

    fn set_subfield(&mut self, mut attrs: Attributes) {
        if let Ok(code) = get_code(&mut attrs) {
            self.path[5] = code;
        }
    }
}

impl PartialEq for FieldPath {
    fn eq(&self, other: &Self) -> bool {
        for i in 0..self.path.len() {
            if self.path[i] != 0 && other.path[i] != 0 && self.path[i] != other.path[i] {
                return false;
            }
        }
        true
    }
}

impl Eq for FieldPath {}

impl fmt::Display for FieldPath {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        unsafe {
            let mut field = String::from_utf8_unchecked(self.path[0..3].to_owned());
            if self.path[3] != 0 {
                field.push_str(str::from_utf8_unchecked(&self.path[3..=4]));
                field.push_str("$");
                field.push_str(str::from_utf8_unchecked(&[self.path[5]]));
            }
            write!(f, "{}", field)
        }
    }
}

trait ValueCache {
    fn add(&mut self, pattern: String, value: String) -> ();

    fn add_rec_id(&mut self, rec_id: String) -> ();

    fn has_content(&self) -> bool;

    fn flush(&mut self) -> String;
}

struct PatternValues {
    table: HashMap<String, Vec<String>>,
    count: bool,
    rec_id: Option<String>,
}

impl PatternValues {
    fn new(count: bool) -> Self {
        PatternValues {
            table: HashMap::new(),
            count,
            rec_id: None,
        }
    }
}

impl ValueCache for PatternValues {
    fn add(&mut self, pattern: String, value: String) {
        let value_list = self.table.entry(pattern).or_insert_with(|| vec![]);
        (*value_list).push(value);
    }

    fn add_rec_id(&mut self, rec_id: String) {
        self.rec_id = Some(rec_id);
    }

    fn has_content(&self) -> bool {
        !self.table.is_empty()
    }

    fn flush(&mut self) -> String {
        let mut print = String::with_capacity(128);
        for (i, (key, values)) in self.table.iter().enumerate() {
            if i > 0 {
                print.push_str("\n");
            }
            if self.count {
                if let Some(id) = &self.rec_id {
                    print.push_str(&id);
                }
                print.push_str("\t");
                print.push_str(&key);
                print.push_str("\t");
                print.push_str(format!("{}", values.len()).as_ref());
            } else {
                for (j, value) in values.iter().enumerate() {
                    if j > 0 {
                        print.push_str("\n");
                    }
                    if let Some(id) = &self.rec_id {
                        print.push_str(&id);
                    }
                    print.push_str("\t");
                    print.push_str(&key);
                    print.push_str("\t");
                    print.push_str(value);
                }
            }
        }
        self.table.clear();
        self.rec_id = None;
        print.push_str("\n");
        print
    }
}

struct RecordValues {
    rec_id: Option<String>,
    table: HashMap<String, u16>,
    column_order: Vec<String>,
}

impl RecordValues {
    fn new(column_order: Vec<String>) -> Self {
        RecordValues {
            rec_id: None,
            table: HashMap::new(),
            column_order,
        }
    }
}

impl ValueCache for RecordValues {
    fn add(&mut self, pattern: String, _value: String) {
        let counter = self.table.entry(pattern).or_insert(0);
        *counter += 1;
    }

    fn add_rec_id(&mut self, rec_id: String) {
        self.rec_id = Some(rec_id);
    }

    fn has_content(&self) -> bool {
        !self.table.is_empty()
    }

    fn flush(&mut self) -> String {
        let mut print = String::with_capacity(64);
        if let Some(id) = &self.rec_id {
            print.push_str(&id);
        }
        for col in self.column_order.iter() {
            print.push_str("\t");
            if self.table.contains_key(col) {
                print.push_str(&format!("{}", self.table.get(col).unwrap()));
            } else {
                print.push_str("0");
            }
        }
        self.table.clear();
        self.rec_id = None;
        print.push_str("\n");
        print
    }
}

enum FieldType {
    LEADER,
    CONTROLFIELD,
    DATAFIELD,
    SUBFIELD,
    OTHER,
}
