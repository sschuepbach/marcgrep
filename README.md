# marcgrep

Command line tool for grepping MARC-XML records

## Installation

In order to build the application from source, [Rust](https://rust-lang.org) and [cargo](https://doc.rust-lang.org/stable/cargo/) are required. Follow the[instructions](https://www.rust-lang.org/tools/install) on the official Rust website if you haven't installed them yet. Afterwards you can proceed with

```sh
# Clone from remote repository
git clone https://gitlab.com/sschuepbach/marcgrep.git
# Change to directory
cd marcgrep
# Install to local cargo bin folder (directory should be in $PATH, otherwise marcgrep can't be found by the terminal)
cargo install -path .
```

## Usage

```sh
marcgrep [FLAGS] <PATTERNS>
```

`marcgrep` reads MARC-XML records from `STDIN` expecting one record per line. It scans the record for one or more patterns and prints the contents of the matching (sub)fields in one of the following forms:

- As values (flag `-v`, `--value`)
- As counts, one pattern per line (flag `-c`, `--count`)
- As counts, one record per line (flag `-t`, `--tabular`)

The indvidual elements on one line are tab-separated.

A pattern can either be a full path to a (sub)field like `LDR`, `003`, `1001 a` (notice the missing `$` sign and the whitespace) or a part of a path. You can use question marks (`?`) to add a wildcard for a single position or simply leave out trailing characters to encompass a larger range of (sub)fields. For instance:

- `100`: Matches all subfields and indicator combinations of field `100`
- `00`: Matches all controlfields (i.e. fields beginning with `00`)
- `100? `: Matches all subfields in field `100` with an empty second indicator and arbitrary subfield code
- `1000?a`: Matches all subfields in field `100` with first indicator equals `0` and subfield code `a`

_Caution_: Wildcards and partial parts don't work with the tabular (flag `-t`) representation of data. In this case a pattern is taken as-is, i.e. a pattern like `100` won't match anything! The reason for this behaviour is the stream-based approach of reading the input data: The application can't foresee every existing instance of a partial / wildcard pattern in only a single pass, so a tabular representation is not possible.

Typically, you will print a MARC-XML collection with either `(z)cat` or `curl` and pipe `STDOUT` to `marcgrep`. Each representation of matching (sub)fields in marcgrep has its one purposes. A few hints:

- value representation (`-v`): Check the correctness of values of certain (sub)fields
- count representation (`-c`): Count the total of records comprising a certain amount of a (sub)field
- tabular representation (`-t`): Check if certain combinations of (sub)fields exist in records; show records which don't contain a certain field as well

Useful tools to postprocess the data include `awk`, `sed`, `cut`, `grep`, `wc`, `sort` and `uniq`.

## Examples

### Filter combinations with awk

```sh
zcat an_example_file.xml.gz | marcgrep -t '1001 a' '7001 a' | awk '$2>0 && $3<1 {print $0}'
```

In this example, awk prints only lines in which the element in the second (tab-separated) column (= value of subfield `1001 a`) is greater than 0 and the element in the third column (= value of subfield `7001 a`) is lesser than 1.